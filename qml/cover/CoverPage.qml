import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

CoverBackground {
    id:cover

    Connections {
        target: mainController
        onStatusChanged: {
            statusText.text=text
        }
        onSumUnreadChanged: {
            numUnread.text=num
        }
    }
    Column {
        anchors {
            left: parent.left
            right: parent.right
            top:parent.top
        }
        Row {
            anchors {
                left: parent.left
                right: parent.right
                margins:Theme.paddingLarge
            }
            Text {
                id:numUnread
                text:'0'
                color:Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
            }
            Text {
                anchors.bottom:numUnread.bottom
                text: qsTr(" new messages")
                color:Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
            }
        }
        Separator {
            width:parent.width
            color:Theme.primaryColor
            height: 0.5*Theme.paddingSmall
        }
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: '[matrix]'
        font.pixelSize: Theme.fontSizeExtraLarge
        color:Theme.secondaryColor
    }

    Column {
        id:bottom
        anchors {
            left: parent.left
            right: parent.right
            bottom:parent.bottom
            margins: Theme.paddingLarge
        }
        Text {
            id:statusText
            anchors.right:bottom.right
            text:qsTr('starting')
            color:Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
        }
    }




}
