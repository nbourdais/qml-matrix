import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import Nemo.DBus 2.0

import "pages"

import "./js/Settings.js" as MySettingsJs
import "./js/MatrixClient.js" as MatrixClientJs
import "controller"

ApplicationWindow
{
    id: applicationWindow
    initialPage: Component { IndexPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations:  Orientation.Portrait

    property var matrixClient: new MatrixClientJs.MatrixClient(applicationWindow)
    property var settings: new MySettingsJs.Settings()

    MainController {
        id:mainController
    }


    Notification {
        id: messageNotification
        category: "x-nemo.messaging.im"
        urgency: Notification.Critical
        appIcon: "image://theme/icon-lock-chat"
        appName: "QML-Matrix"
        summary: ""
        body: "Notification body"
        maxContentLines: 5
        remoteActions: [ {
            name: "default",
            service: "harbour.qmlmatrix.service",
            path: "/harbour/qmlmatrix/service",
            iface: "harbour.qmlmatrix.service",
            method: "showApp",
            arguments: [ ]
        } ]
    }

    DBusAdaptor {
        service: "harbour.qmlmatrix.service"
        iface: "harbour.qmlmatrix.service"
        path: "/harbour/qmlmatrix/service"
        xml: '<interface name="harbour.qmlmatrix.service"><method name="openPage"/></interface>'

        function showApp() {
            applicationWindow.activate()
        }

        /*function openPage(roomid, roomname) {
            __silica_applicationwindow_instance.activate()
            pageStack.pop(null, true)
            pageStack.push(Qt.resolvedUrl("pages/RoomPage.qml"), { room_id: roomid, room_name: roomname } )
        }*/
    }
}
