function MatrixClient(applicationWindow) {

    this.applicationWindow=applicationWindow
    this.homeserver=''
    this.username =''
    this.accessToken=''
    this.roomList=[]
    this.userList=[]
    this.nextBatch=''
    this.numUnreadLastSync={}

    /*
      clear values
      */
    this.clearValues=function() {
        this.homeserver=''
        this.username =''
        this.accessToken=''
        this.roomList=[]
        this.userList=[]
        this.nextBatch=''
    }

    /*
      Login with username and password
     */
    this.login=function(username,password,success,failed) {
        var mc=this
        mc.username=username
        var url = this.homeserver +'/_matrix/client/r0/login'
        var params = {
            'type':'m.login.password',
            'user':username,
            'password':password,
            'device_id':'sailfish-qml-matrix',
            'initial_device_display_name':'Sailfish QML-MAtrix'
        }
        mc.doHttpRequest('POST',url,params,
            function(msg) {
                if (msg.access_token) {
                    mc.accessToken=msg.access_token;
                    success(mc.accessToken);
                }
                else failed({errorCode:0,error:'missing access token',httpStatus:200})
            },
            failed
        )
    }

    /*
     * Logout from session
     */
    this.logout=function() {
        var mc=this
        var url = this.homeserver +'/_matrix/client/r0/logout'
        var params={}
        this.doHttpRequest('POST',url,params, this.clearValues, this.clearValues)
    }


    /*
     * Do sync
     */
    this.sync=function(firstSync,timeout,success,failed) {
        var mc=this
        var url=this.homeserver+'/_matrix/client/r0/sync'
        var params={}
        //define timout if not initial sync
        if (!firstSync) {
            params.timeout=timeout
            params.since=this.nextBatch
        }
        //start sync request
        var result=this.doHttpRequest('GET',url,params, function(result) {
            var changedRooms=[]
            var accountDataMDirect
            //Parse account data info
            if (result.account_data && result.account_data.events) {
                for (var i=0; i<result.account_data.events.length; i++) {
                    var event=result.account_data.events[i];
                    //direct chats
                    if (event.type==="m.direct") accountDataMDirect=event.content
                }
            }
            //reset counters for new messages
            mc.numUnreadLastSync['favourite']=0
            mc.numUnreadLastSync['direct']=0
            mc.numUnreadLastSync['room']=0
            //Parse room info
            if (result.rooms && result.rooms.join) {
                //Add all joinedRooms
                for (var id in result.rooms.join) {
                    changedRooms.push(id)
                    var syncJoinedRoom=result.rooms.join[id]
                    var isNewRoom=false
                    var room=mc.getRoom(id)
                    if (!room) {
                        isNewRoom=true
                        room=new MatrixRoom(id)
                    }
                    else room.isInvite=false
                    //parse contents
                    mc.parseSyncRoom(syncJoinedRoom,room)
                    //add room if new
                    console.log("MatrixClient: Joined room: "+room.name+" - "+room.id+"; isNew: "+isNewRoom)
                    if (isNewRoom) mc.roomList.push(room);
                }
                //Add invitations
                for (var id in result.rooms.invite) {
                    var syncInviteRoom=result.rooms.invite[id]
                    var room=new MatrixRoom(id)
                    mc.parseSyncRoom(syncInviteRoom,room)
                    if (room.isInvite) {
                        mc.roomList.push(room)
                        changedRooms.push(id)
                        console.log("MatrixClient: Invitation to room: "+room.name+" - "+room.id)
                    }
                }
            }
            //Handle direct chat rooms (if account_data with type m.direct present)
            if (accountDataMDirect) {
                for (var i=0; i<mc.roomList.length; i++) {
                    var room=mc.roomList[i]
                    var isDirect=false
                    for (var userid in accountDataMDirect) {
                        var roomid=accountDataMDirect[userid][0]
                        if (room.id===roomid) {
                            console.log("MatrixClient: Direct chat with "+userid+" in room "+roomid)
                            isDirect=true
                            var user=mc.getUser(userid)
                            if (user && !user.name) user.name=userid.substring(1,userid.indexOf(':'))
                            if (user) {
                                room.name=user.name
                                room.avatar=user.avatar
                            }
                            else {
                                room.name=userid.substring(1,userid.indexOf(':'))
                            }
                        }
                    }
                    //check if state is changed
                    if (room.isDirect!==isDirect && changedRooms.indexOf(roomid)<0) {
                        changedRooms.push(roomid)
                    }
                    room.isDirect=isDirect
                }
            }

            //Sort rooms by number of unread messages and name
            mc.roomList.sort(compareRooms)
            //Next Batch Token
            mc.nextBatch=result.next_batch
            console.log('MatrixClient: Number of changes: '+changedRooms.length)
            success(changedRooms);
        },
        //error handling
        failed)
    }


    /*
     Send text message
    */
    this.sendTextMessage=function(roomid,text,success,failed) {
        var mc=this
        var url = mc.homeserver +'/_matrix/client/r0/rooms/'+roomid+'/send/m.room.message'
        var params = {
            msgtype:"m.text",
            body:text
        }
        mc.doHttpRequest('POST',url,params, success, failed)
    }

    /*
     Upload file and send message
     */
    this.uploadAndSendMessage=function(roomid,properties,msgtype,success,failed) {
        var mc=this
        mc.uploadFile(properties,
            function(content_uri) {
                var url = mc.homeserver +'/_matrix/client/r0/rooms/'+roomid+'/send/m.room.message'
                var params = {
                    msgtype:msgtype,
                    body:properties.fileName,
                    url:content_uri
                }
                mc.doHttpRequest('POST',url,params, success, failed)
            },
            failed
        )
    }

    /*
      Mark all messages of a room as read
      */
    this.markRoomAsRead=function(roomid) {
        var room=this.getRoom(roomid)
        var num=room.timeline.length
        if (num>0) {
            room.numUnread=0
            var url = this.homeserver +'/_matrix/client/r0/rooms/'+room.id+'/receipt/m.read/'+room.timeline[num-1].id
            this.doHttpRequest('POST',url,{},function (msg) {},function (msg) {})
        }
    }

    /*
      Toggle favourite status of a room
      */
    this.toggleFavourite=function(roomid,success,failed) {
        var room=this.getRoom(roomid)
        var url = this.homeserver +'/_matrix/client/r0/user/'+this.getMyMatrixId()+'/rooms/'+room.id+'/tags/m.favourite'
        var params={order:'0.5'}
        if (room.isFavourite) {
            this.doHttpRequest('DELETE',url,{},
                function (msg) {room.isFavourite=false; success(msg)},
                failed
            )
        }
        else {
            this.doHttpRequest('PUT',url,params,
               function (msg) {room.isFavourite=true; success(msg)},
               failed
           )
        }
    }

    /*
      Redact message
      */
    this.redactMessage=function(roomid,messageid,reason,success,failed) {
        if (!reason) reason=qsTr('unspecified')
        var room=this.getRoom(roomid)
        var url= this.homeserver+'/_matrix/client/r0/rooms/'+roomid+'/redact/'+messageid+'/'+messageid
        var params={reason:reason}
        this.doHttpRequest('PUT',url,params,
            function(msg) {
                success(msg)
            },
            failed
        )
    }

    /*
      Join Room
     */
    this.joinRoom=function(roomid,success,failed) {
        var mc=this
        var url = this.homeserver+'/_matrix/client/r0/rooms/'+roomid+'/join'
        this.doHttpRequest('POST',url,{},success,failed)
    }

    /*
      Leave Room
     */
    this.leaveRoom=function(roomid,success,failed) {
        var mc=this
        var url = this.homeserver +'/_matrix/client/r0/rooms/'+roomid+'/leave'
        this.doHttpRequest('POST',url,{},
            function(msg) {
                for (var i=0; i<mc.roomList.length; i++) {
                    if (mc.roomList[i].id===roomid) mc.roomList.splice(i,1)
                }
                success(msg)
            },
            failed
        )
    }

    //Create Room
    this.createRoom=function(name, topic, type, success, failed) {
        var mc=this
        var url = this.homeserver +'/_matrix/client/r0/createRoom'
        var params={ name:name, topic:topic, preset: type }
        this.doHttpRequest('POST',url,params,success,failed)
    }

    //Create Direct Room
    this.createDirectRoom=function(invitee, success, failed) {
        var mc=this
        var url = this.homeserver +'/_matrix/client/r0/createRoom'
        var params={preset: "trusted_private_chat", invite: [invitee], is_direct: true}
        this.doHttpRequest('POST',url,params,function(msg) {
            var roomid=msg.room_id
            //get current account data for m.direct
            url=mc.homeserver+'/_matrix/client/r0/user/'+mc.getMyMatrixId()+'/account_data/m.direct'
            mc.doHttpRequest('GET',url,{},function(msg) {
                //add room and save account data
                var url=mc.homeserver+'/_matrix/client/r0/user/'+mc.getMyMatrixId()+'/account_data/m.direct'
                msg[invitee]=[roomid]
                console.log(JSON.stringify(msg))
                mc.doHttpRequest('PUT',url,msg,success,failed)
            },failed)
        },failed)
    }

    /*
      Invite member
      */
    this.inviteMember=function(roomid,member,success,failed) {
        var url= this.homeserver+'/_matrix/client/r0/rooms/'+roomid+'/invite'
        var params = { user_id: member }
        this.doHttpRequest('POST',url,params,success,failed)
    }

    /*
      Download File
      */
    this.downloadFile=function(url,fileName,dirName,success,failed) {
        var command='/usr/bin/curl'
        var params=[
            '--output',dirName+'/'+fileName,
            encodeURI(url)
        ]
        var process = Qt.createQmlObject("import QtQuick 2.0; import Process 1.0; Process { onReadyReadStandardError: console.log(readAllStandardError()) }",this.applicationWindow,"curlProcess");
        console.log('MatrixClient: running '+command+JSON.stringify(params))
        process.start(command,params)
        //TODO error handling of process
        success()
    }

    /*
      Upload file
      */
    this.uploadFile=function(properties,success,failed) {
        var mc=this
        var url=this.homeserver+'/_matrix/media/r0/upload?filename='+properties.fileName
        var command='/usr/bin/curl'
        var params=[
            '--data-binary','@'+properties.filePath,
            '--header','Content-Type: '+properties.mimeType,
            '--header','Authorization: Bearer '+mc.accessToken,
            '--request','POST',
            encodeURI(url)
        ]
        //var process=applicationWindow.process
        var process = Qt.createQmlObject("import QtQuick 2.0; import Process 1.0; Process { onReadyReadStandardError: console.log(readAllStandardError()) }",this.applicationWindow,"curlProcess");
        process.onReadyRead.connect(function() {
            var output=process.readAll()
            try {
                var result=JSON.parse(output)
                if (result.content_uri) success(result.content_uri)
                else failed(result)
            }
            catch (e) {
                console.error(e)
                failed({error:qsTr('Unknown error during upload, perhaps the filesize is to large.')})
            }
            process.destroy()
        })
        process.onError.connect(function() {
            failed({error:'Upload failed'})
            process.destroy()
        })
        console.log('MatrixClient: running '+command+JSON.stringify(params))
        process.start(command,params)
    }

    //load old messages
    this.loadOldMessages=function(roomid,success,failed) {
        var mc=this
        var room=this.getRoom(roomid)
        var url=this.homeserver+'/_matrix/client/r0/rooms/'+room.id+'/messages'
        var params = { from : room.prevBatch, dir: 'b', limit: settings.get('limitOldMessages') }
        this.doHttpRequest('GET',url,params,function(msg) {
            var chunk=msg.chunk
            try {
                if (chunk.length==0) {
                    failed(qsTr('No more older messages available.'))
                }
                else {
                    for (var i=0; i<chunk.length; i++) {
                        var message=mc.parseSyncEvent(chunk[i])
                        if (message) room.addMessage(message)
                    }
                    room.timelineMajorChange=true
                    room.prevBatch=msg.end
                    success({})
                }
            }
            catch (e) {
                console.error(e)
                failed({error:qsTr('Unknown error while loading old messages.')})
            }
        },
        failed)
    }

    //Set account data
    this.putAccountData=function(type,key,value,success,failed) {
        var url=this.homeserver+'/_matrix/client/r0/user/'+this.getMyMatrixId()+'/account_data/'+type
        var params={}
        params[key]=value
        console.log(JSON.stringify(params))
        this.doHttpRequest('PUT',url,params,success,failed)

    }

    //Simple getter methods
    this.getHomeserver=function() { return this.homeserver }
    this.getRoomList=function() { return this.roomList }

    //My Matrix Id
    this.getMyMatrixId =function() {
        var matrixId='@'+this.username+':'+this.homeserver.substring(this.homeserver.indexOf('://')+3)
        return matrixId
    }

    //Get Room
    this.getRoom=function(id) {
        for (var i=0; i<this.roomList.length; i++) {
            if (this.roomList[i].id===id) return this.roomList[i]
        }
        return null
    }

    //Get User
    this.getUser=function(id) {
        for (var i=0; i<this.userList.length; i++) {
            if (this.userList[i].id===id) return this.userList[i]
        }
        return null
    }

    //Number of unread messages
    this.getSumUnread=function() {
        var sum=0
        for (var i=0; i<matrixClient.roomList.length; i++) {
            sum+=matrixClient.roomList[i].numUnread
        }
        return sum
    }

    /* --------------------------------------- internal methods*/

    //Add User
    this.addUser=function(user) {
        var newUser=true;
        for (var i=0; i<this.userList.length && newUser; i++) {
            if (this.userList[i].id===user.id) newUser=false
        }
        if (newUser) this.userList.push(user)
    }

    //Parsing
    this.parseSyncRoom=function(sync, room) {
        //Parse state infos
        if (sync.state && sync.state.events) {
            for (var i=0; i<sync.state.events.length; i++) {
                var event=sync.state.events[i];
                if (event.type==="m.room.create") room.creator=event.content.creator
                if (event.type==="m.room.name") room.name=event.content.name
                if (event.type==="m.room.avatar") room.avatar=event.content.url
                if (event.type==="m.room.topic") room.topic=event.content.topic
                if (event.type==="m.room.member") {
                    var user=new MatrixUser(event.state_key)
                    user.avatar=event.content.avatar_url
                    user.name=event.content.displayname
                    this.addUser(user)
                    room.addMember(user.id)
                }
            }
            //Check if favourite
            if (sync.account_data && sync.account_data.events) {
                for (var i=0; i<sync.account_data.events.length; i++) {
                    var event=sync.account_data.events[i]
                    if (event.type==='m.tag' && event.content.tags['m.favourite']) room.isFavourite=true
                }
            }
        }
        //Parse invite state infos
        if (sync.invite_state && sync.invite_state.events) {
            for (var i=0; i<sync.invite_state.events.length; i++) {
                var event=sync.invite_state.events[i];
                if (event.type==="m.room.join_rules") room.isInvite=true
                if (event.type==="m.room.name") room.name=event.content.name
                if (event.type==="m.room.avatar") room.avatar=event.content.url
                if (event.type==="m.room.topic") room.topic=event.content.topic
                if (event.type==="m.room.member" && event.content.membership==="join") {
                    var user=new MatrixUser(event.state_key)
                    user.avatar=event.content.avatar_url
                    user.name=event.content.displayname
                    this.addUser(user)
                    room.addMember(user.id)
                }
            }
        }

        //Parse timeline infos
        if (sync.timeline && sync.timeline.prev_batch) room.prevBatch=sync.timeline.prev_batch
        if (sync.timeline && sync.timeline.events) {
            if (!room.timeline) room.timeline=[]
            for (var i=0; i<sync.timeline.events.length; i++) {
                var event=sync.timeline.events[i];
                //meta infos
                if (event.type==="m.room.name") room.name=event.content.name
                if (event.type==="m.room.topic") room.topic=event.content.topic

                //redaction
                if (event.type==="m.room.redaction") {
                    var reason=qsTr('unknown')
                    if (event.content && event.content.reason) reason=event.content.reason
                    var redactid=event.redacts
                    room.redactMessage(redactid,reason)
                }
                //parse message
                else {
                    var message=this.parseSyncEvent(event)
                    if (message) {
                        room.addMessage(message)
                        //increment message counter
                        if (message.sender!==this.getMyMatrixId()) {
                            if (room.isFavourite) this.numUnreadLastSync['favourite']++
                            else if (room.isDirect) this.numUnreadLastSync['direct']++
                            else this.numUnreadLastSync['room']++
                        }
                    }
                }
            }
        }
        //Parse unread_notifications
        if (sync.unread_notifications && sync.unread_notifications.notification_count) {
            room.numUnread=sync.unread_notifications.notification_count
        }
        else room.numUnread=0
    }

    //Parse event
    this.parseSyncEvent=function(event) {
        if (event.type==="m.room.message") {
            var message=new MatrixMessage(event.event_id)
            message.sender=event.sender
            message.text=event.content.body
            message.html=convertUrls(message.text)
            message.ts=event.origin_server_ts
            //text message
            if (event.content.msgtype==="m.text" || event.content.msgtype==='m.notice') {
                message.type='text'
                return message
            }
            //image
            else if (event.content.msgtype==="m.image") {
                message.type='image'
                message.url=event.content.url
                return message
            }
            //video
            else if (event.content.msgtype==="m.video") {
                message.type='video'
                message.url=event.content.url
                return message
            }
            //file
            else if (event.content.msgtype==="m.file") {
                message.type='file'
                message.url=event.content.url
                return message
            }
            //redaction
            else if (event.unsigned && event.unsigned.redacted_because && event.unsigned.redacted_because.content) {
                message.type='redact'
                message.text=''
                message.html='<i>'+qsTr('redacted')+': '+event.unsigned.redacted_because.content.reason+'</i>'
                return message
            }
            //unknown
            else {
                console.log('MatrixClient: Unsupported m.room.message event: '+JSON.stringify(event))
            }

        }

        //encryption not supported
        else if (event.type==="m.room.encrypted") {
            var message=new MatrixMessage(event.event_id);
            message.sender=event.sender
            message.ts=event.origin_server_ts
            message.type='encrypted'
            message.text=qsTr('Encryption not yet supported ...')
            message.html=message.text
            return message
        }
        return null
    }

    this.mxc2url=function(mxc) {
        if (!mxc || mxc==='') return ''
        var url=this.homeserver+'/_matrix/media/r0/download/'+mxc.substring(6)
        return url
    }

    this.mxc2thumb=function(mxc,size) {
        if (!mxc || mxc==='') return ''
        var url=this.homeserver+'/_matrix/media/r0/thumbnail/'+mxc.substring(6)+'?width='+size+'&height='+size
        return url
    }

    this.mxc2preview=function(mxc,width) {
        if (!mxc || mxc==='') return ''
        var url=this.homeserver+'/_matrix/media/r0/download/'+mxc.substring(6)+'?width=640&height=480&method=scale'
        return url
    }

    //Send async HTTP request
    this.doHttpRequest=function(method,url,params,success,failed) {
        //determine timeout (default 15s)
        var timeout=15000
        if (params && params['timeout']) timeout+=params['timeout']
        //build url with params for get
        if (method.toUpperCase()==='GET') {
            var pos=0;
            for (var key in params) {
                if (pos++===0) url+='?'
                else url+='&'
                url+=key+'='+params[key]
            }

        }
        //log request
        console.log('MatrixClient: '+method+' '+url)//+'?access_token='+this.accessToken)

        //create request objec
        var xhr = new XMLHttpRequest()

        //timer for terminating the request (attribut .timeout not supported in qml)
        var timer = Qt.createQmlObject("import QtQuick 2.0; Timer {interval: "+timeout+"; repeat: false; running: true;}",this.applicationWindow,"getRequestTimer");
            timer.triggered.connect(function(){ xhr.abort(); });

        //listen to response
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                timer.running = false;
                console.log('MatrixClient: STATUS '+xhr.status)
                if (xhr.status === 200) {
                    var responseJson=JSON.parse(xhr.responseText)
                    success(responseJson)
                }
                else if (xhr.status >= 0) {
                    try {
                        var responseJson=JSON.parse(xhr.responeText)
                        failed({error:responseJson.error, httpStatus:xhr.status })
                    }
                    catch (e) {
                        var response={httpStatus:xhr.status, error:qsTr('HTTP-error ')+xhr.status}
                        if (xhr.status===400) response.error+=qsTr(': bad request')
                        if (xhr.status===401) response.error+=qsTr(': not authenticated')
                        if (xhr.status===403) response.error+=qsTr(': access forbidden')
                        if (xhr.status===524) response.error+=qsTr(': timeout error')
                        failed(response)
                    }
                }
                else {
                    failed({errorCode:0, error:qsTr('No connection possible'),httpStatus:0})
                }

            }
        }

        //start request
        xhr.open(method.toUpperCase(), url, true);
        if (this.accessToken) xhr.setRequestHeader("Authorization","Bearer "+this.accessToken)
        if (method.toUpperCase==='GET' || params==={}) xhr.send()
        else xhr.send(JSON.stringify(params))
    }

}

/*
  Compare to rooms - for sorting
  */
function compareRooms( a, b ) {
  if ( a.numUnread > b.numUnread) return -1;
  else if (a.numUnread < b.numUnread) return +1;
  else if (!b.name) return -1
  else if (!a.name) return +1
  else if (a.name.toLowerCase()>b.name.toLowerCase()) return 1
  else if (a.name.toLowerCase()<b.name.toLowerCase()) return -1
  else return 0
}

/*
  Search hyperlinks in text messages
  */
function convertUrls(text) {
    if (!text) return ''
    //replace line breaks
    text=text.replace(new RegExp('\r?\n','g'), '<br>');
    //replace urls
    var urlPattern = /(https:|http:|www.)[^ ]{1,1000}/
    return text.replace(urlPattern, function(match, offset, string) {
        var href=match
        var name=match
        //Ggf. Protokoll für Link ergänzen
        if (href.indexOf('www')===0) href='http://'+href
        //Ggf. in der Anzeige Protokoll entfernen
        if (name.indexOf('https://')===0) name=name.substring(8)
        if (name.indexOf('http://')===0) name=name.substring(7)
        //Normaler Link
        if (name.indexOf('/')>0) name=name.substring(0,name.indexOf('/'))
        return '<a href="'+href+'">'+name+'</a>'
    })
}


/*
  Representation of a Matrix room
  */
function MatrixRoom(roomid) {
    this.id=roomid
    this.name='?'
    this.creator=''
    this.topic=''
    this.avatar=''
    this.numUnread=0
    this.timeline=[]
    this.members=[]
    this.isDirect=false
    this.isFavourite=false
    this.isInvite=false
    this.prevBatch=''
    this.timelineMajorChange=false


    this.addMember=function(userid) {
        var newMember=true
        for (var i=0; i<this.members.length && newMember; i++) {
            if (this.members[i]===userid) newMember=false
        }
        if (newMember) this.members.push(userid)
    }

    this.addMessage=function(message) {
        var newMessage=true
        for (var i=0; i<this.timeline.length && newMessage; i++) {
            if (this.timeline[i].id===message.id) newMessage=false
        }
        if (newMessage && message.id) {
            if (this.timeline[0] && message.ts<=this.timeline[0].ts) {
                this.timeline.unshift(message)
            }
            else {
                this.timeline.push(message)
            }
        }
    }

    this.redactMessage=function(messageid,reason) {
        for (var i=0; i<this.timeline.length; i++) {
            if (this.timeline[i].id===messageid) {
                this.timelineMajorChange=true
                var message=this.timeline[i]
                message.type='redact'
                message.html='<i>'+qsTr('redacted')+': '+reason+'</i>'
                message.txt=''
                message.url=''
            }
        }
    }
}


/*
  Representation of a Matrix user
  */
function MatrixUser(id) {
    this.id=id
    this.name='?'
    this.avatar=''
}

/*
  Representation of a Matrix Message
  */
function MatrixMessage(id) {
    this.id=id
    this.type=''
    this.text=''
    this.html=''
    this.sender=''
    this.url=''
    this.ts=0
}
