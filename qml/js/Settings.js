function Settings() {

    this.values={}

    this.defaultValues={
        homeserver: 'https://matrix.org',
        userid: '',
        accessToken: '',
        syncTimeout: '60000',
        syncExtraDelay: '0',
        syncErrorDelay: '30000',
        limitOldMessages: '30'
    }

    this.init=function(callback) {
        var values=this.values
        this.db = LocalStorage.openDatabaseSync("QmlMatrix", "1.0", "Local storage for QML_MATRIX", 10000);

        this.db.transaction(
            function(tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS Settings(key TEXT, value TEXT)');
                var rs=tx.executeSql('SELECT key, value FROM Settings');
                for (var i = 0; i < rs.rows.length; i++) {
                    var key=rs.rows.item(i).key;
                    var value=rs.rows.item(i).value;
                    console.log('Setting '+key+ " = "+value)
                    values[key]=value;
                }
                callback()
            }
        )
    }


    this.get=function(key) {
        if (this.values[key]) return this.values[key]
        var value=this.defaultValues[key]
        console.log('Warning: Missing setting with key '+key+'; using default '+value)
        return value;
    }

    this.getInt=function(key) {
        return(parseInt(this.get(key)))
    }

    this.getBool=function(key) {
           return(this.get(key)==='true')
    }

    this.set=function(key,value) {
        this.values[key]=value
        this.db.transaction(
            function(tx) {
                var rs=tx.executeSql('DELETE FROM Settings WHERE key=?',[key]);
                rs=tx.executeSql('INSERT INTO Settings VALUES(?, ?)',[key,value]);
            }
        )
    }
}
