import QtQuick 2.0

QtObject {
    signal uploadFinished
    signal firstSyncDone
    signal statusChanged(string text)
    signal sumUnreadChanged(int num)
    signal roomsChanged(var changedRooms)
    signal messagesChanged(var roomid)
    signal messageSent(var roomid)

    property bool online: false
    property string infoText: ''
    property int syncErrorDelay:1000

    property Timer showInfoTimer:
        Timer {
            interval: 100
            running: false
            repeat: true
            onTriggered: {
                if (!pageStack.busy) {
                    showInfoTimer.stop()
                    pageStack.push(Qt.resolvedUrl("../pages/InfoPage.qml"),{infoText:infoText})
                }
            }
        }

    property Timer syncDelayTimer:
        Timer {
            repeat:false
            running: false
            onTriggered: nextSync()
        }


    function firstSync() {
        settings.init(function() {
            var accessToken=settings.get('accessToken')
            if (!accessToken) {
                pageStack.replace(Qt.resolvedUrl("../pages/LoginPage.qml"))
            }
            else {
                var username=settings.get('username')
                var homeserver=settings.get('homeserver')
                matrixClient.homeserver=homeserver
                matrixClient.username=username
                matrixClient.accessToken=accessToken
                matrixClient.sync(true,0,
                    function() {
                        firstSyncDone()
                        online=true
                        nextSync()
                    },
                    //error handling
                    function(msg) {
                        if (msg.httpStatus===0) pageStack.replace(Qt.resolvedUrl("../pages/CriticalError.qml"),{errorText:qsTr('Cant connect to homeserver. You have to be online on application startup.')})
                        else pageStack.replace(Qt.resolvedUrl("../pages/LoginPage.qml"))
                    }
                 )
             }
        })
    }

    /*
      Start Sync Loop
      */
    function nextSync() {
        console.log('MainController: next sync started...')
        statusChanged(qsTr('online – syncing'))

        try {
            matrixClient.sync(false,settings.getInt('syncTimeout'),
                function(changedRooms){
                    online=true
                    syncErrorDelay=1000 //reset delay time fo errors
                    if (changedRooms.length>0) {
                        //display changes on main page
                        roomsChanged(changedRooms)
                        //display new messsages
                        for (var i=0; i<changedRooms.length; i++) {
                            messagesChanged(changedRooms[i])
                        }

                        //update info for cover
                        var sumUnread=matrixClient.getSumUnread()
                        sumUnreadChanged(sumUnread)
                        //show system notification
                        var notify= (matrixClient.numUnreadLastSync['favourite']>0 && settings.getBool('notifyOnFavourite')) ||
                                    (matrixClient.numUnreadLastSync['direct']>0 && settings.getBool('notifyOnDirect')) ||
                                    (matrixClient.numUnreadLastSync['room']>0 && settings.getBool('notifyOnRoom'))
                        if (notify && sumUnread>0) {
                            messageNotification.summary=qsTr('New messages: '+sumUnread)
                            messageNotification.previewSummary=qsTr('New messages: '+sumUnread)
                            messageNotification.body=qsTr("Click to open messages")
                            messageNotification.previewBody=qsTr("Click to open messages")
                            messageNotification.publish()
                        }
                    }
                    //extraDelay if configured and not active
                    var extraDelay=settings.getInt('syncExtraDelay')
                    if (extraDelay>0 && Qt.application.state!==Qt.ApplicationActive) {
                        statusChanged(qsTr('online – sleeping '))
                        syncDelayTimer.stop()
                        syncDelayTimer.interval=extraDelay
                        syncDelayTimer.start()
                    }
                    else {
                        nextSync()
                    }
                },
                //error handling
                function(msg){
                    online=false
                    //increase delay time on error - limit by syncErrorDelay setting
                    syncErrorDelay=Math.min(syncErrorDelay*2,settings.getInt('syncErrorDelay'))
                    var delay=Math.max(2000,syncErrorDelay)
                    //connection failed (without error)
                    if (msg.httpStatus===0) statusChanged(qsTr('offline - no connection'))
                    //access denied or not authorized -> login
                    else if (msg.httpStatus===401 || msg.httpStatus===403)  pageStack.replace(Qt.resolvedUrl("../pages/LoginPage.qml"))
                    //timout
                    else if (msg.httpStatus===524) delay=0
                    //unknown error -> show code
                    else statusChanged(qsTr('offline – code')+' '+msg.httpStatus)
                    //retry after specified intervall
                    if (delay>0) {
                        syncDelayTimer.stop()
                        syncDelayTimer.interval=delay
                        console.log('MainController: next sync delayed by '+delay+'ms')
                        syncDelayTimer.start()
                    }
                    else nextSync()
                }
            )
        }
        //catch exceptions (which should never occur...)
        catch (e) {
            console.error(e)
            statusChanged(qsTr('error - '+e))
        }
    }

    /*
      Show Room
      */
    function showRoom(roomid) {
        wakeUp()
        var room=matrixClient.getRoom(roomid)
        var roomPage=pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { "room":room })
        matrixClient.markRoomAsRead(room.id)
    }

    // Load old messages for room
    function loadOldMessages(roomid) {
        matrixClient.loadOldMessages(roomid, function() {messagesChanged(roomid)}, showInfo)
    }

    /*
      Toggle Favourite
      */
    function toggleFavourite(roomid) {
        if (!online) return showInfo(qsTr('You need to be online for this action.'))
        matrixClient.toggleFavourite(roomid,nop,showInfo)
    }

    /*
      Leave Room
      */
    function leaveRoom(roomid) {
        if (!online) return showInfo(qsTr('You need to be online for this action.'))
        matrixClient.leaveRoom(roomid, function() {roomsChanged([roomid])}, showInfo)
    }


    //Invite new member
    function inviteMember(roomid,member) {
        matrixClient.inviteMember(roomid,member,nop,showInfo)
    }

    /*
      Join Room
      */
    function joinRoom(roomid) {
        if (!online) return showInfo(qsTr('You need to be online for this action.'))
        matrixClient.joinRoom(roomid, function() {roomsChanged([roomid])}, showInfo)
    }

    //Create new room
    function createRoom(name,topic,type) {
        if (!online) return showInfo(qsTr('You need to be online for this action.'))
        matrixClient.createRoom(name, topic, type, function(msg) {roomsChanged(msg.room_id)}, showInfo)
    }

    //Create new room
    function createDirectRoom(invitee) {
        if (!online) return showInfo(qsTr('You need to be online for this action.'))
        matrixClient.createDirectRoom(invitee, function(msg) {roomsChanged(msg.room_id)}, showInfo)
    }

    //Send text message
    function sendTextMessage(roomid,text) {
        if (!online) return showInfo(qsTr('You have to be online for this action.'))
        matrixClient.sendTextMessage(roomid,text,function() {messageSent(roomid)},showInfo)
    }

    //Send media message
    function sendMediaMessage(roomid,properties,type) {
        matrixClient.uploadAndSendMessage(roomid,properties,type,
            uploadFinished,
            showInfo
        )
    }

    //Redact Message
    function redactMessage(roomid,messageid,reason) {
        if (!online) return showInfo(qsTr('You have to be online for this action.'))
        matrixClient.redactMessage(roomid, messageid, reason, nop, showInfo)
    }


    //Download file
    function downloadFile(url,fileName,dirName) {
        if (!online) return showInfo(qsTr('You have to be online for this action.'))
        matrixClient.downloadFile(url,fileName,dirName,
            function(msg) {
                showInfo(qsTr('Download started in background.'))
            },
            showInfo
        )
    }

    //Mark room as read
    function markRoomAsRead(roomid) {
        matrixClient.markRoomAsRead(roomid)
    }

    /*
      Logout from server
      */
    function logout() {
        statusChanged(qsTr('logged out'))
        matrixClient.logout();
        settings.set('accessToken','')
        pageStack.replace(Qt.resolvedUrl("../pages/LoginPage.qml"))
    }

    /*
      Stop sleep & error state
      */
    function wakeUp() {
        console.log('MainController: WakeUp')
        if (syncDelayTimer.running) {
            syncDelayTimer.stop()
            nextSync()
        }
    }

    /*
      Signal Info message
      */
    function showInfo(msg) {
        infoText=''
        if (msg.error) infoText=qsTr('Could not perform the action:'+'\n\n'+msg.error)
        else infoText=msg
        showInfoTimer.stop()
        showInfoTimer.start()
    }

    /*
      Dummy - do nothing, for empty callbacks
      */
    function nop() {
    }

}

