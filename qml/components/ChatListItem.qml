import QtQuick 2.0
import Sailfish.Silica 1.0

Component {
    ListItem {
        height:Math.max(Theme.itemSizeMedium,rowMessage.height)+rowDay.height+Theme.paddingSmall
        Column {
            width:roomPage.width
            spacing: Theme.paddingMedium

            //Day - only visible if day is given
            Column {
                visible: day!=null
                id:rowDay
                height:(day ? 1.1*Theme.fontSizeLarge +3 :0)
                width:parent.width
                //Date text
                Text {
                   width:parent.width
                   text:day+' '
                   color:Theme.secondaryColor
                   horizontalAlignment: Text.AlignRight
                   font.pixelSize: Theme.fontSizeMedium*1.1
                   anchors {
                       left: parent.left
                       right: parent.right
                       margins: Theme.paddingMedium
                   }
                }
                //Decoration line
                Rectangle {
                    width:parent.width
                    height:3
                    color:(day ? Theme.secondaryColor : 'transparent')
                }
            }

            Row {
                id:rowMessage
                width:parent.width
                height:Math.max(Theme.itemSizeMedium,colText.height)
                spacing: Theme.paddingSmall
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                property var sender: matrixClient.getUser(message.sender)

                //Avatar item
                Column {                    
                    width:Theme.itemSizeMedium
                    AvatarItem {
                        name:rowMessage.sender.name
                        url: rowMessage.sender.avatar || ''
                        numUnread: 0
                    }
                }

                //Name, time and message text
                Column {
                    id:colText
                    width:rowMessage.width-Theme.itemSizeMedium-Theme.paddingMedium
                    spacing: Theme.paddingSmall
                    Row {
                        height:Theme.fontSizeMedium
                        width:parent.width
                        //Sender
                        Text {
                            width:parent.width*0.5
                            text:rowMessage.sender.name
                            color:Theme.highlightColor
                            font.pixelSize: Theme.fontSizeMedium
                        }
                        //Time
                        Text {
                            width:parent.width*0.5
                            text:time
                            color:Theme.secondaryColor
                            font.pixelSize: Theme.fontSizeMedium
                            horizontalAlignment: Text.AlignRight
                        }
                    }


                    //Image
                    Image {
                        visible: message.type==='image'
                        width:parent.width
                        fillMode: Image.PreserveAspectFit
                        source: message.type==='image' ? matrixClient.mxc2thumb(message.url,parent.width,parent.width) : ''
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {Qt.openUrlExternally(matrixClient.mxc2url(message.url))}
                        }
                    }

                    //Video
                    Button {
                        property string href:matrixClient.mxc2url(message.url)
                        visible: message.type==='video'
                        text: qsTr('Play video')
                        //width : parent.width
                        onClicked: {Qt.openUrlExternally(href)}
                    }


                    //File
                    Button {
                        visible: message.type==='file'
                        text: qsTr('Download file')
                        onClicked: pageStack.push(Qt.resolvedUrl("../pages/ConfirmDownloadDialog.qml"), { url:matrixClient.mxc2url(message.url), name:message.text })
                    }

                    //Message text
                    Text {
                        width:parent.width
                        text: (message.html) ? "<style>a:link {color: " + Theme.highlightColor + "; }</style>"+message.html : ''
                        color:Theme.primaryColor
                        wrapMode: TextEdit.WordWrap
                        font.pixelSize: Theme.fontSizeMedium
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.RichText
                    }
                }
            }
        }

        menu: ContextMenu {
            MenuItem {
                text: qsTr("Redact message")
                onClicked: pageStack.push(Qt.resolvedUrl("../pages/RedactMessagePage.qml"), { room:room, message:message })
            }

            MenuItem {
                text: qsTr("Copy message")
                onClicked: Clipboard.text=message.text
            }
        }
    }
}


