import QtQuick 2.0
import Sailfish.Silica 1.0

Button {
    width:parent.width
    text:''
    anchors {
        left: parent.left
        right: parent.right
        margins: Theme.paddingLarge
    }
}
