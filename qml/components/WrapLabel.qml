import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    anchors {
        left: parent.left
        right: parent.right
        margins: Theme.paddingLarge
    }
    wrapMode:Text.Wrap
}
