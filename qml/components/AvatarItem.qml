import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    width: Theme.itemSizeMedium
    height: Theme.itemSizeMedium

    property string url
    property string name
    property int numUnread
    Rectangle {
        width: parent.height
        height: parent.width
        color: Theme.secondaryColor

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            text:(name) ? name.substring(0,1) +name.substring(name.length-1) : '?'
            font.pixelSize: 0.7*Theme.itemSizeMedium
            font.weight: Font.Bold
            color: Theme.primaryColor
        }

        Image {
            width:parent.width
            height:parent.width
            source: matrixClient.mxc2thumb(url,Theme.itemSizeMedium)
            fillMode:Image.PreserveAspectCrop
        }

        Rectangle {
            width: (numUnread>0) ? Theme.fontSizeSmall :0
            height: (numUnread>0) ? Theme.fontSizeSmall :0
            anchors.right: parent.right
            anchors.top: parent.top
            color:Theme.highlightBackgroundColor

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text:  (numUnread>0) ? numUnread : ''
                color:Theme.primaryColor
            }
        }

    }
}
