import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    property string titleText: ''
    property int numUnread: -1
    width:parent.width
    height:Theme.fontSizeLarge
    color: "transparent"

    Label {
        anchors.left: Theme.paddingMedium
        height:Theme.fontSizeLarge
        width:parent.width-3*Theme.fontSizeLarge
        text: titleText
    }
    Label {
        anchors.right: parent.right
        height:Theme.fontSizeLarge
        text: numUnread
    }
}
