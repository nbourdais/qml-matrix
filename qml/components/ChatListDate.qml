import QtQuick 2.0
import Sailfish.Silica 1.0

Component {
    ListItem {
        height:Theme.fontSizeLarge
        Text {
            width:parent.width
            text:date
            color:Theme.primaryColor
            font.pixelSize: Theme.fontSizeMedium
        }
    }
}


