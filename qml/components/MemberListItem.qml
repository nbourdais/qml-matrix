import QtQuick 2.0
import Sailfish.Silica 1.0
import '../components'


ListItem {
    width:Theme.itemSizeMedium

    Column {
        width:parent.width
        AvatarItem {
            name:user.name ? user.name : '?'
            url:user.avatar ? user.avatar : ''

            MouseArea {
                anchors.fill: parent
                onPressAndHold: openMenu()
            }

        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: user.name ? user.name : '?'
            color: Theme.primaryColor
            clip: true
        }
    }


}



