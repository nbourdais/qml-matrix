import QtQuick 2.0
import Sailfish.Silica 1.0
import '../components'


ListItem {
    width:Theme.itemSizeMedium
    menu: ContextMenu {
        MenuItem {
            text: "Toggle favourite"
            onClicked: mainController.toggleFavourite(room.id)
        }
        MenuItem {
            text: "Leave room"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/ConfirmLeaveRoomDialog.qml"), { "room":room })
        }
        /*MenuItem {
            text: "Edit details"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/EditRoomDialog.qml"), { "room":room })
        }*/
        MenuItem {
            text: "Room members"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/RoomMembersPage.qml"), { "room":room })
        }

    }

    Column {
        width:parent.width
        AvatarItem {
            name:room.name ? room.name : '?'
            url:room.avatar ? room.avatar : ''
            numUnread: room.numUnread

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (!room.isInvite) mainController.showRoom(room.id)
                    else pageStack.push(Qt.resolvedUrl("../pages/ConfirmJoinRoomDialog.qml"), { "room":room })
                }
                onPressAndHold: {
                    if (!room.isInvite) openMenu()
                }
            }

        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: room.name ? room.name : '?'
            color: Theme.primaryColor
            clip: true
        }
    }


}



