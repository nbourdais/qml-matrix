import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var room

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Join Room")
            }

            WrapLabel {
                text:qsTr("Please confirm that you want to join the following room:")+' '+room.name
            }

        }
    }

    onAccepted: mainController.joinRoom(room.id)
}
