import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property var errorText
    SilicaFlickable {
        anchors.fill: parent
        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                id:header
                title: qsTr("Error")
            }

            WrapLabel {
                text:errorText
            }

            WrapLabel {
                text:qsTr('Please close the application.')
            }

        }
    }
}
