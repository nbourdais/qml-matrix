import QtQuick 2.5
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import "../components"


Page {
    property var room
    property Timer timer:timer
    Connections {
        target: mainController
        onUploadFinished: {
            info2.visible=true
            timer.start()
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        Column {
            id:columnMenu
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                id:header
                title: qsTr("Upload")
            }

            WideButton {
                id:button1
                text:qsTr('Upload image')
                onClicked: pageStack.push(imagePickerPage)
            }

            WideButton {
                id:button3
                text:qsTr('Upload video')
                onClicked: pageStack.push(videoPickerPage)
            }

            WideButton {
                id:button2
                text:qsTr('Upload file')
                onClicked: pageStack.push(filePickerPage)
            }

            Image {
                id: image
                visible: false
                source: ""
                width: 0.5*parent.width
                fillMode: Image.PreserveAspectFit
                cache: false
                autoTransform: true
            }

            WrapLabel {
                id:info1
                text: qsTr('Upload in progress - please wait...')
                visible: false
            }

            WrapLabel {
                id:info2
                text: qsTr('... completed!')
                visible: false
            }
        }
    }

    Timer {
        id:timer
        interval: 1000; running: false; repeat: false
        onTriggered: pageStack.pop()
    }

    Component {
        id: imagePickerPage
        ImagePickerPage {
            onSelectedContentPropertiesChanged: {
                var src = selectedContentProperties.filePath
                image.source=src
                image.visible=true
                info1.visible=true
                mainController.sendMediaMessage(room.id,selectedContentProperties,'m.image')
            }
        }
    }

    Component {
        id: filePickerPage
        FilePickerPage {
            onSelectedContentPropertiesChanged: {
                info1.visible=true
                mainController.sendMediaMessage(room.id,selectedContentProperties,'m.file')
            }
        }
    }

    Component {
        id: videoPickerPage
        VideoPickerPage {
            onSelectedContentPropertiesChanged: {
                info1.visible=true
                mainController.sendMediaMessage(room.id,selectedContentProperties,'m.video')
            }
        }
    }
}
