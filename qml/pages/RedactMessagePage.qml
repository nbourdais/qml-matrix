import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var message
    property var room

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Redact message")
            }

            TextField {
                id: reason
                width: page.width
                text: ''
                label: qsTr("Reason")
                placeholderText: label
            }
        }
    }

    onAccepted: mainController.redactMessage(room.id,message.id,reason.text)

}
