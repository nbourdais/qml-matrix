import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"
import "../model"


Page {
    id: mainPage
    property string coverText: ''
    property Column columnLoading: columnLoading
    property Column columnRooms: columnRooms
    property Text statusText: statusText

    Connections {
        target: mainController
        onFirstSyncDone: {
            columnLoading.visible=false
            columnRooms.visible=true
            displayRooms()
        }
        onStatusChanged: {
            statusText.text=text
        }
        onRoomsChanged: {
            coverText=mainController.sumUnread+' '+qsTr('new\nmessages')
            //TODO only display changes
            displayRooms()
        }
    }

    function displayRooms() {
        var allRooms=matrixClient.getRoomList();
        favouriteList.model.clear();
        directList.model.clear();
        elseList.model.clear();
        inviteList.model.clear();

        var numUnreadFavouite=0
        var numUnreadDirect=0
        var numUnreadElse=0
        var numUnreadInvite=0


        //display Rooms sorted by type
        for (var i=0; i< allRooms.length; i++) {
            var room=allRooms[i]
            if (room.isFavourite) {
                numUnreadFavouite+=room.numUnread
                favouriteList.model.append({room:room})
            }

            else if (room.isDirect) {
                numUnreadDirect+=room.numUnread
                directList.model.append({room:room, roomName:room.name, roomAvatar:room.avatar})
            }
            else if (room.isInvite) {
                numUnreadInvite+=room.numUnread
                inviteList.model.append({room:room, roomName:room.name, roomAvatar:room.avatar})
            }
            else {
                numUnreadElse+=room.numUnread
                elseList.model.append({room:room, roomName:room.name, roomAvatar:room.avatar})
            }

        }

        //give summary information on unread messages
        favouriteTitle.numUnread=numUnreadFavouite
        directTitle.numUnread=numUnreadDirect
        elseTitle.numUnread=numUnreadElse
        inviteTitle.numUnread=numUnreadInvite
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            /*MenuItem {
                text: qsTr('Join existing room')
                onClicked: pageStack.push(Qt.resolvedUrl("JoinRoomDialog.qml"))
            }*/
            MenuItem {
                text: qsTr('Create new room')
                onClicked: pageStack.push(Qt.resolvedUrl("CreateRoomDialog.qml"))
            }
            MenuItem {
                text: qsTr('Start direct chat')
                onClicked: pageStack.push(Qt.resolvedUrl("CreateDirectRoomDialog.qml"))
            }

            MenuItem {
                text: qsTr("Reconnect")
                visible:!(mainController.online)
                onClicked: mainController.wakeUp()
            }

            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }


            MenuItem {
                text: qsTr("Logout")
                onClicked: {
                    mainController.logout();
                }
            }
        }

        Column {
            id:columnLoading
            width: parent.width
            PageHeader {
                title: qsTr("Overview")
            }
            WrapLabel {
                text: qsTr('Please wait - loading rooms...')
            }

        }

        Column {
            id:columnRooms
            visible: false
            x:Theme.paddingMedium
            width: mainPage.width-2*Theme.paddingMedium
            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("Overview")
            }

            RoomListTitle {
                id: favouriteTitle
                titleText: qsTr('Favourites')
            }


            SilicaListView {
                id: favouriteList
                flickableDirection: Flickable.HorizontalFlick
                boundsBehavior: Flickable.StopAtBounds
                orientation: ListView.Horizontal
                height:Theme.itemSizeMedium+1*Theme.fontSizeMedium+2*Theme.paddingMedium
                width:parent.width;
                model: RoomListModel {}
                delegate: RoomListItem {}
                spacing: Theme.paddingMedium
                clip:true
            }

            RoomListTitle {
                id: directTitle
                titleText: qsTr('Direct Chats')
            }

            SilicaListView {
                id: directList
                flickableDirection: Flickable.HorizontalFlick
                boundsBehavior: Flickable.StopAtBounds
                orientation: ListView.Horizontal
                height:Theme.itemSizeMedium+1*Theme.fontSizeMedium+2*Theme.paddingMedium
                width:parent.width;
                model: RoomListModel {}
                delegate: RoomListItem {}
                spacing: Theme.paddingMedium
                clip:true
            }

            RoomListTitle {
                id: elseTitle
                titleText: qsTr('Rooms')
            }

            SilicaListView {
                id: elseList
                flickableDirection: Flickable.HorizontalFlick
                boundsBehavior: Flickable.StopAtBounds
                orientation: ListView.Horizontal
                height:Theme.itemSizeMedium+1*Theme.fontSizeMedium+2*Theme.paddingMedium
                width:parent.width;
                model: RoomListModel {}
                delegate: RoomListItem {}
                spacing: Theme.paddingMedium
                clip:true
            }

            RoomListTitle {
                id: inviteTitle
                titleText: qsTr('Invitations')
                visible:inviteList.model.count>0
            }

            SilicaListView {
                id: inviteList
                visible:model.count>0
                flickableDirection: Flickable.HorizontalFlick
                boundsBehavior: Flickable.StopAtBounds
                orientation: ListView.Horizontal
                height:Theme.itemSizeMedium+1*Theme.fontSizeMedium+2*Theme.paddingMedium
                width:parent.width;
                model: RoomListModel {}
                delegate: RoomListItem {}
                spacing: Theme.paddingMedium
                clip:true
            }

        }
    }

    //Status bar at bottom
    Rectangle {
        width:parent.width
        height:Theme.fontSizeMedium
        anchors.bottom: parent.bottom
        color: "transparent"
        Text {
            color:Theme.primaryColor
            id: statusText
            width:parent.width
            text:qsTr('loading')
            horizontalAlignment: Text.AlignRight
        }
    }

    //wait until settings from LoginPage saved
    Timer {
        interval: 500; running: true; repeat: false
        onTriggered: {
            mainController.firstSync()
        }
    }


}
