import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../js/MatrixClient.js" as MatrixClient;

Page {
    id: page

    function doLogin() {
        matrixClient.homeserver=inputHomeserver.text
        matrixClient.login(inputUsername.text,inputPassword.text,
            function(accessToken) {
                //async functions - hence next page hast to wait some time...
                settings.set('homeserver',inputHomeserver.text)
                settings.set('username',inputUsername.text)
                settings.set('accessToken',accessToken)
                pageStack.replace(Qt.resolvedUrl("IndexPage.qml"))
            },
            function(msg) {
                labelLoginFailed.text=qsTr('Login failed with reason:')+'\n'+msg.error
                labelLoginFailed.visible=true
            }
        )
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Login Account")
            }

            Label {
                id: labelLoginFailed
                visible: false
                padding: Theme.paddingLarge
            }

            TextField {
                id: inputHomeserver
                width: page.width
                focus: true
                label: qsTr("Homeserver")
                text: settings.get('homeserver')
                placeholderText: label
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: inputUsername.focus = true
            }

            TextField {
                id: inputUsername
                width: page.width
                text: settings.get('username')
                label: qsTr("Username")
                placeholderText: label
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: inputPassword.focus = true
            }

            PasswordField {
                id: inputPassword
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {doLogin()}
            }

            Button {
                id: buttonLogin
                text: qsTr("Login")
                 anchors.horizontalCenter: parent.horizontalCenter
                 onClicked: {doLogin()}
             }


        }
    }


}
