import QtQuick 2.0
import Sailfish.Silica 1.0
import "../model"
import "../components"



Page {
    id:roomPage
    property string name:'roomMembersPage'
    property var room
    property var coverAvatar: room
    property var coverText: room.name

    Connections {
        target: mainController
        onRoomsChanged: {
            showAllMembers()
        }
    }

    function showAllMembers() {
        memberList.model.clear()
        for (var i=0; i<room.members.length; i++) {
            var userid=room.members[i]
            var user=matrixClient.getUser(userid)
            memberList.model.append({user:user})
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                text: qsTr('Invite new member')
                onClicked: pageStack.push(Qt.resolvedUrl("InviteMemberPage.qml"),{room:room})
            }
        }

        Column {
            id: column
            width: roomPage.width
            height:roomPage.height
            spacing: Theme.paddingLarge
            PageHeader {
                id:header
                title: qsTr('Members of room')+' '+room.name
                height:Theme.iconSizeLarge
            }

            SilicaGridView {
                id:memberList
                width: parent.width
                height: 6*Theme.itemSizeMedium
                cellWidth: Theme.itemSizeMedium+2*Theme.paddingMedium
                cellHeight: Theme.itemSizeMedium+Theme.fontSizeMedium+2*Theme.paddingMedium
                model: MemberListModel {}
                delegate: MemberListItem {}
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
            }

        }
    }

    Component.onCompleted: {
        showAllMembers()
    }
}
