import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var room

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Invite member")
            }

            TextField {
                id: textNewMember
                width: page.width
                focus: true
                label: qsTr("Full matrix address")
                placeholderText: '@member:homeserver.org'
            }

            WrapLabel {
                id: info
                visible:false
            }

        }
    }

    onAccepted: {
        mainController.inviteMember(room.id,textNewMember.text)
    }

}
