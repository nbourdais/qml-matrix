import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("Settings")
            }

            WrapLabel {
                text:qsTr('Notification options')
            }

            TextSwitch {
                id: notifyOnFavourite
                text: qsTr('favourites messages')
                checked: settings.getBool('notifyOnFavourite')
                onCheckedChanged: settings.set('notifyOnFavourite', notifyOnFavourite.checked ? 'true' : 'false')
            }

            TextSwitch {
                id: notifyOnDirect
                text: qsTr('direct chat messages')
                checked: settings.getBool('notifyOnDirect')
                onCheckedChanged: settings.set('notifyOnDirect', notifyOnDirect.checked ? 'true' : 'false')
            }

            TextSwitch {
                id: notifyOnRoom
                text: qsTr('room messages')
                checked: settings.getBool('notifyOnRoom')
                onCheckedChanged: settings.set('notifyOnRoom', notifyOnRoom.checked ? 'true' : 'false')
            }

            Separator {
                color:Theme.primaryColor
                width:parent.width
                height: Theme.paddingSmall
            }

            WrapLabel {
                text:qsTr('Timing options')
            }

            Slider {
                id: syncTimeout
                width: parent.width
                minimumValue: 10
                maximumValue: 300
                label: qsTr('Timeout Sync')
                valueText:value+'s'
                stepSize: 10
                value: settings.get('syncTimeout')/1000
                onValueChanged: settings.set('syncTimeout',syncTimeout.value*1000)
            }

            Slider {
                id: syncErrorDelay
                width: parent.width
                minimumValue: 10
                maximumValue: 300
                label: qsTr('Retry timeout on errors')
                valueText:value+'s'
                stepSize: 10
                value: settings.get('syncErrorDelay')/1000
                onValueChanged: settings.set('syncErrorDelay',syncErrorDelay.value*1000)
            }

            Slider {
                id: syncExtraDelay
                width: parent.width
                minimumValue: 0
                maximumValue: 300
                label: qsTr('Extra timeout between syncs')
                valueText:value+'s'
                stepSize: 10
                value: settings.get('syncExtraDelay')/1000
                onValueChanged: settings.set('syncExtraDelay',syncExtraDelay.value*1000)
            }

        }
    }


}
