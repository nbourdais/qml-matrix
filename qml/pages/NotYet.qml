import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Information")
            }

            WrapLabel {
                text:qsTr("Sorry, this feature isn't available at the moment.")
            }

        }
    }
}
