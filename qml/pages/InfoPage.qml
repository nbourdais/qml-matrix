import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property var infoText
    SilicaFlickable {
        anchors.fill: parent
        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                id:header
                title: qsTr("Information")
            }

            WrapLabel {
                text:infoText
            }

            WideButton {
                text:qsTr('Close')
                onClicked: pageStack.pop()
            }
        }
    }
}
