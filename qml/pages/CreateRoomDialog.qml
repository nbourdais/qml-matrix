import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var room

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Create new room")
            }

            TextField {
                id: roomName
                width: page.width
                focus: true
                label: qsTr("Room name")
                placeholderText: label
            }

            TextField {
                id: roomTopic
                width: page.width
                focus: true
                label: qsTr("Room topic")
                placeholderText: label
            }

            ComboBox {
                id: roomType
                 width:parent.width
                 label: qsTr("Room type")

                 menu: ContextMenu {
                     MenuItem { property string type: "private_chat";  text: qsTr("Private chat") }
                     MenuItem { property string type: "trusted_private_chat"; text: qsTr("Trusted private chat") }
                     MenuItem { property string type: "public_chat"; text: qsTr("Public chat") }
                 }
             }

        }
    }

    onAccepted: {
        mainController.createRoom(roomName.text, roomTopic.text, roomType.currentItem.type)
    }

}
