import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var url
    property var name

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Download file")
            }

            TextField {
                id: inputName
                width: page.width
                focus: true
                label: qsTr("File name")
                text: name
            }

            TextField {
                id: inputDir
                width:page.width
                label: qsTr("Directory")
                text: StandardPaths.download
            }

            WrapLabel {
                text:qsTr("If the file already exists, it will be overridden without further confirmation.")
            }

        }
    }

    onAccepted: {
        mainController.downloadFile(url,inputName.text,inputDir.text)
    }
}
