import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Dialog {
    id: page
    property var room

    SilicaFlickable {
        anchors.fill: parent


        Column {
            id:column
            width: parent.width
            spacing: Theme.paddingMedium

            DialogHeader {
                id:header
                title: qsTr("Create direct chat")
            }

            TextField {
                id: invitee
                width: page.width
                focus: true
                label: qsTr("Invitee matrix address")
                placeholderText: label
            }
        }
    }

    onAccepted: {
        mainController.createDirectRoom(invitee.text)
    }

}
