import QtQuick 2.0
import Sailfish.Silica 1.0
import "../model"
import "../components"



Page {
    id:roomPage
    property var room
    property var lastDay: ''
    property var displayedMessageIds:[]
    property var coverAvatar
    property var coverText: ''

    Connections {
        target: mainController
        onMessageSent: {
            if (roomid===room.id) text.text=''
        }

        onMessagesChanged: {
            if (roomid===room.id) {
//                room=matrixClient.getRoom(roomid)
                var timeline=room.timeline
                if (room.timelineMajorChange) {
                    showAllMessages()
                    room.timelineMajorChange=false
                }
                else {
                    for (var j=0; j<timeline.length; j++) {
                        appendMessage(timeline[j])
                    }
                }
                //update cover
                coverAvatar={ name:room.name, avatar:room.avatar, numUnread:room.numUnread }

                //mark last message as receipt if active
                if (Qt.application.state===Qt.ApplicationActive) mainController.markRoomAsRead(room.id)
            }
        }
    }

    function showAllMessages() {
        console.log('Showing all Messages')
        chatList.model.clear()
        displayedMessageIds=[]
        for (var i=0; i<room.timeline.length; i++) {
            appendMessage(room.timeline[i])
        }
    }

    function appendMessage(message) {
        //do nothing, if message is already visible
        if (displayedMessageIds.indexOf(message.id)>=0) return
        //format date and time
        var thisDate=new Date(message.ts)
        var thisDay=thisDate.toLocaleDateString(Locale.LongFormat)
        var thisTime=thisDate.toLocaleTimeString(Locale.ShortFormat)
        //show date only if changed
        if (lastDay!==thisDay) {
            chatList.model.append({message:message, day:thisDay, time:thisTime})
            lastDay=thisDay
        }
        //display messages
        else {
            chatList.model.append({message:message, day:'', time:thisTime})
        }
        displayedMessageIds.push(message.id)
        if (!room.timelineMajorChange) chatList.scrollToBottom()
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                text: qsTr('Invite new member')
                onClicked: pageStack.push(Qt.resolvedUrl("InviteMemberPage.qml"),{room:room})
            }
            MenuItem {
                text: qsTr('Load older messages')
                onClicked: mainController.loadOldMessages(room.id)
            }
        }

        Column {
            id: column
            width: roomPage.width
            height:roomPage.height
            spacing: Theme.paddingLarge
            PageHeader {
                id:header
                title: room.name
                height:Theme.iconSizeLarge
            }
            SilicaListView {
                id: chatList
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds
                orientation: ListView.Vertical
                height:roomPage.height-header.height-Theme.paddingLarge-3*Theme.fontSizeMedium
                width:parent.width;
                model: ChatListModel {}
                delegate: ChatListItem {}
                spacing: Theme.paddingMedium
                clip: true
                VerticalScrollDecorator { flickable: ListView }
            }


            Row {
                width:parent.width
                height:3*Theme.fontSizeMedium

                Column {
                    width:parent.width-Theme.iconSizeMedium-Theme.paddingMedium
                    height:parent.height
                    TextArea {
                        id:text
                        width: parent.width-Theme.iconSizeMedium
                        focus: false
                        placeholderText: qsTr("Enter message or send file")
                        wrapMode: TextEdit.WordWrap
                        onTextChanged: {
                            if (text.text) button.text='▷'
                            else button.text='📎'
                        }
                     }

                }
                Column {
                    width:Theme.iconSizeMedium
                    height:parent.height
                    Button {
                        visible:mainController.online
                        id:button
                        width:Theme.iconSizeMedium
                        height:Theme.iconSizeMedium
                        onClicked: {
                            //text message
                            if (text.text.length>0) {
                                mainController.sendTextMessage(room.id,text.text)
                            }
                            //upload
                            else {
                                pageStack.push(Qt.resolvedUrl("UploadPage.qml"),{room:room})
                            }
                        }
                        text: '📎'
                    }
                }
            }

        }
    }

    Component.onCompleted: {
        showAllMessages()
    }
}
