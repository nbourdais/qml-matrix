# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-qml-matrix

CONFIG += sailfishapp

SOURCES += \
    src/harbour-qml-matrix.cpp

DISTFILES += \
    harbour-qml-matrix.desktop \
    qml/components/AvatarItem.qml \
    qml/components/ChatListItem.qml \
    qml/components/MemberListItem.qml \
    qml/components/RoomListItem.qml \
    qml/components/RoomListTitle.qml \
    qml/components/WideButton.qml \
    qml/components/WrapLabel.qml \
    qml/controller/MainController.qml \
    qml/cover/CoverPage.qml \
    qml/harbour-qml-matrix.qml \
    qml/js/MatrixClient.js \
    qml/js/Settings.js \
    qml/model/ChatListModel.qml \
    qml/model/MemberListModel.qml \
    qml/model/RoomListModel.qml \
    qml/pages/ConfirmDownloadDialog.qml \
    qml/pages/ConfirmJoinRoomDialog.qml \
    qml/pages/ConfirmLeaveRoomDialog.qml \
    qml/pages/CreateDirectRoomDialog.qml \
    qml/pages/CreateRoomDialog.qml \
    qml/pages/CriticalError.qml \
    qml/pages/IndexPage.qml \
    qml/pages/InfoPage.qml \
    qml/pages/InviteMemberPage.qml \
    qml/pages/LoginPage.qml \
    qml/pages/NotYet.qml \
    qml/pages/RedactMessagePage.qml \
    qml/pages/RoomMembersPage.qml \
    qml/pages/RoomPage.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/UploadPage.qml \
    rpm/qml-matrix.changes.in \
    rpm/qml-matrix.changes.run.in \
    rpm/qml-matrix.spec \
    rpm/qml-matrix.yaml \
    translations/*.ts

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-qml-matrix-de.ts translations/harbour-qml-matrix-fr.ts

HEADERS += \
    src/process.h

RESOURCES +=


