Basic Matrix client for Sailfish OS - written in pure QML without external libraries

This client is under **heavy** development, currently only basic things will work.

# Overview functions

- send and receive unencrypted messages (text, image, video, file)
- send and receive redactions
- send and receive invitations
- set favourites
- create and leave rooms

Hopefully, in a couple of weeks I will add

- edit room settings
- basic profile options (change avatar, password)

Not planned (as I have no idea):

- encryption
- presence tracking

# Compiling

You need the Sailfish SDK in order to compile the app:

- Install Sailfish SDK
- Import project bei opening the *.pro file
- Click on the build button -> armv7 -> Release -> Build a RPM...

Unfortunately I don't have the time to provide binary packages on a regular base. If you would like to contribute, please don't hesitate to contact me.

Have a look at the wiki to find (a perhaps outdated) package.