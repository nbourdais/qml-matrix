<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ChatListItem</name>
    <message>
        <location filename="../qml/components/ChatListItem.qml" line="102"/>
        <source>Play video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ChatListItem.qml" line="111"/>
        <source>Download file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ChatListItem.qml" line="131"/>
        <source>Redact message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ChatListItem.qml" line="136"/>
        <source>Copy message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmDownloadDialog</name>
    <message>
        <location filename="../qml/pages/ConfirmDownloadDialog.qml" line="21"/>
        <source>Download file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ConfirmDownloadDialog.qml" line="28"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ConfirmDownloadDialog.qml" line="35"/>
        <source>Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ConfirmDownloadDialog.qml" line="40"/>
        <source>If the file already exists, it will be overridden without further confirmation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmJoinRoomDialog</name>
    <message>
        <location filename="../qml/pages/ConfirmJoinRoomDialog.qml" line="20"/>
        <source>Join Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ConfirmJoinRoomDialog.qml" line="24"/>
        <source>Please confirm that you want to join the following room:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmLeaveRoomDialog</name>
    <message>
        <location filename="../qml/pages/ConfirmLeaveRoomDialog.qml" line="20"/>
        <source>Leaving Room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ConfirmLeaveRoomDialog.qml" line="24"/>
        <source>Please confirm that you really want to leave the following room:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> new messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="68"/>
        <source>starting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateDirectRoomDialog</name>
    <message>
        <location filename="../qml/pages/CreateDirectRoomDialog.qml" line="20"/>
        <source>Create direct chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateDirectRoomDialog.qml" line="27"/>
        <source>Invitee matrix address</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateRoomDialog</name>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="20"/>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="27"/>
        <source>Room name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="35"/>
        <source>Room topic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="42"/>
        <source>Room type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="45"/>
        <source>Private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="46"/>
        <source>Trusted private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoomDialog.qml" line="47"/>
        <source>Public chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CriticalError</name>
    <message>
        <location filename="../qml/pages/CriticalError.qml" line="17"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CriticalError.qml" line="25"/>
        <source>Please close the application.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IndexPage</name>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="25"/>
        <source>new
messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="83"/>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="87"/>
        <source>Start direct chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="92"/>
        <source>Reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="98"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="104"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="115"/>
        <location filename="../qml/pages/IndexPage.qml" line="131"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="118"/>
        <source>Please wait - loading rooms...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="136"/>
        <source>Favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="155"/>
        <source>Direct Chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="173"/>
        <source>Rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="191"/>
        <source>Invitations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/IndexPage.qml" line="222"/>
        <source>loading</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../qml/pages/InfoPage.qml" line="17"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/InfoPage.qml" line="25"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InviteMemberPage</name>
    <message>
        <location filename="../qml/pages/InviteMemberPage.qml" line="20"/>
        <source>Invite member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/InviteMemberPage.qml" line="27"/>
        <source>Full matrix address</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="20"/>
        <source>Login failed with reason:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="39"/>
        <source>Login Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="52"/>
        <source>Homeserver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="63"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="77"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainController</name>
    <message>
        <location filename="../qml/controller/MainController.qml" line="57"/>
        <source>Cant connect to homeserver. You have to be online on application startup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="70"/>
        <source>online – syncing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="95"/>
        <location filename="../qml/controller/MainController.qml" line="96"/>
        <source>Click to open messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="103"/>
        <source>online – sleeping </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="119"/>
        <source>offline - no connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="125"/>
        <source>offline – code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="163"/>
        <location filename="../qml/controller/MainController.qml" line="171"/>
        <location filename="../qml/controller/MainController.qml" line="185"/>
        <location filename="../qml/controller/MainController.qml" line="191"/>
        <location filename="../qml/controller/MainController.qml" line="197"/>
        <source>You need to be online for this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="203"/>
        <location filename="../qml/controller/MainController.qml" line="217"/>
        <location filename="../qml/controller/MainController.qml" line="224"/>
        <source>You have to be online for this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="227"/>
        <source>Download started in background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/controller/MainController.qml" line="242"/>
        <source>logged out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MatrixClient</name>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="231"/>
        <source>unspecified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="346"/>
        <source>Unknown error during upload, perhaps the filesize is to large.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="368"/>
        <source>No more older messages available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="382"/>
        <source>Unknown error while loading old messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="500"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="578"/>
        <source>Encryption not yet supported ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="643"/>
        <source>HTTP-error </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="644"/>
        <source>: bad request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="645"/>
        <source>: not authenticated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="646"/>
        <source>: access forbidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="647"/>
        <source>: timeout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="652"/>
        <source>No connection possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/js/MatrixClient.js" line="562"/>
        <location filename="../qml/js/MatrixClient.js" line="752"/>
        <source>redacted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotYet</name>
    <message>
        <location filename="../qml/pages/NotYet.qml" line="19"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/NotYet.qml" line="23"/>
        <source>Sorry, this feature isn&apos;t available at the moment.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedactMessagePage</name>
    <message>
        <location filename="../qml/pages/RedactMessagePage.qml" line="21"/>
        <source>Redact message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessagePage.qml" line="28"/>
        <source>Reason</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomMembersPage</name>
    <message>
        <location filename="../qml/pages/RoomMembersPage.qml" line="37"/>
        <source>Invite new member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomMembersPage.qml" line="49"/>
        <source>Members of room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="79"/>
        <source>Invite new member</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="83"/>
        <source>Load older messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="124"/>
        <source>Enter message or send file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="18"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="22"/>
        <source>Notification options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="27"/>
        <source>favourites messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="34"/>
        <source>direct chat messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="41"/>
        <source>room messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <source>Timing options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="61"/>
        <source>Timeout Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="73"/>
        <source>Retry timeout on errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="85"/>
        <source>Extra timeout between syncs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadPage</name>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="27"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="32"/>
        <source>Upload image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="38"/>
        <source>Upload video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="44"/>
        <source>Upload file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="60"/>
        <source>Upload in progress - please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadPage.qml" line="66"/>
        <source>... completed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
