<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ChatListItem</name>
    <message>
        <source>Play video</source>
        <translation>Jouer la vidéo</translation>
    </message>
    <message>
        <source>Download file</source>
        <translation>Télécharger le fichier</translation>
    </message>
    <message>
        <source>Redact message</source>
        <translation>Supprimer le message</translation>
    </message>
    <message>
        <source>Copy message</source>
        <translation>Copier le message</translation>
    </message>
</context>
<context>
    <name>ConfirmDownloadDialog</name>
    <message>
        <source>Download file</source>
        <translation>Télécharger le fichier</translation>
    </message>
    <message>
        <source>File name</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <source>If the file already exists, it will be overridden without further confirmation.</source>
        <translation>Si le fichier existe, il sera écrasé sans autre confirmation.</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="unfinished">Dossier</translation>
    </message>
</context>
<context>
    <name>ConfirmJoinRoomDialog</name>
    <message>
        <source>Join Room</source>
        <translation>Rejoindre le salon</translation>
    </message>
    <message>
        <source>Please confirm that you want to join the following room:</source>
        <translation>Êtes vous certain de vouloir rejoindre ce salon : </translation>
    </message>
</context>
<context>
    <name>ConfirmLeaveRoomDialog</name>
    <message>
        <source>Leaving Room</source>
        <translation>Quitter le salon</translation>
    </message>
    <message>
        <source>Please confirm that you really want to leave the following room:</source>
        <translation>Êtes vous certain de vouloir quitter ce salon : </translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source> new messages</source>
        <translation type="unfinished">Nouveau message</translation>
    </message>
    <message>
        <source>starting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateDirectRoomDialog</name>
    <message>
        <source>Create direct chat</source>
        <translation>Créer un chat direct</translation>
    </message>
    <message>
        <source>Invitee matrix address</source>
        <translation>Adresse matrix de l'invité</translation>
    </message>
</context>
<context>
    <name>CreateRoomDialog</name>
    <message>
        <source>Create new room</source>
        <translation>Créer un nouveau salon</translation>
    </message>
    <message>
        <source>Room name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room topic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trusted private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CriticalError</name>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Please close the application.</source>
        <translation>Veuillez fermer l'application</translation>
    </message>
</context>
<context>
    <name>IndexPage</name>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <source>Please wait - loading rooms...</source>
        <translation>Merci de patienter - chargement des salons...</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Direct Chats</source>
        <translation>Chats directs</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Salons</translation>
    </message>
    <message>
        <source>Invitations</source>
        <translation>Invitations</translation>
    </message>
    <message>
        <source>loading</source>
        <translation>chargement</translation>
    </message>
    <message>
        <source>new
messages</source>
        <translation>nouveaux
Messages</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation>Reconnexion</translation>
    </message>
    <message>
        <source>Start direct chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>InviteMemberPage</name>
    <message>
        <source>Invite member</source>
        <translation>Inviter un membre</translation>
    </message>
    <message>
        <source>Full matrix address</source>
        <translation>Adresse matrix complète</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login Account</source>
        <translation>Compte</translation>
    </message>
    <message>
        <source>Homeserver</source>
        <translation>Homeserver</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d'utilisateur</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Login failed with reason:</source>
        <translation>Erreur de connexion :</translation>
    </message>
</context>
<context>
    <name>MainController</name>
    <message>
        <source>Cant connect to homeserver. You have to be online on application startup.</source>
        <translation>Impossible de se connecter à Homeserver. Merci de vérifier l'accès à internet</translation>
    </message>
    <message>
        <source>Click to open messages</source>
        <translation>Cliquer pour ouvrir les messages</translation>
    </message>
    <message>
        <source>online – sleeping </source>
        <translation>en ligne - au repos </translation>
    </message>
    <message>
        <source>offline - no connection</source>
        <translation>hors ligne - pas de connexion</translation>
    </message>
    <message>
        <source>offline – code</source>
        <translation>hors ligne - erreur</translation>
    </message>
    <message>
        <source>You need to be online for this action.</source>
        <translation>Vous devez être en ligne pour effectuer cette action.</translation>
    </message>
    <message>
        <source>You have to be online for this action.</source>
        <translation>Vous devez être en ligne pour effectuer cette action.</translation>
    </message>
    <message>
        <source>Download started in background.</source>
        <translation>Téléchargement démarré en tâche de fond.</translation>
    </message>
    <message>
        <source>online – syncing</source>
        <translation>en ligne - synchronisation</translation>
    </message>
    <message>
        <source>logged out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MatrixClient</name>
    <message>
        <source>Encryption not yet supported ...</source>
        <translation>Chiffrement pas encore supporté...</translation>
    </message>
    <message>
        <source>redacted</source>
        <translation>supprimé</translation>
    </message>
    <message>
        <source>unspecified</source>
        <translation>non spécifié</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>inconnu</translation>
    </message>
    <message>
        <source>Unknown error during upload, perhaps the filesize is to large.</source>
        <translation>Erreur inconnu pendant l'envoi, le fichier est peut-être trop gros.</translation>
    </message>
    <message>
        <source>: bad request</source>
        <translation>: mauvais requête</translation>
    </message>
    <message>
        <source>: not authenticated</source>
        <translation>: non authentifié</translation>
    </message>
    <message>
        <source>: access forbidden</source>
        <translation>: accès interdit</translation>
    </message>
    <message>
        <source>: timeout error</source>
        <translation>: erreur de timeout</translation>
    </message>
    <message>
        <source>Unknown error while loading old messages.</source>
        <translation>Erreur inconnu pendant le chargement des anciens messages.</translation>
    </message>
    <message>
        <source>No more older messages available.</source>
        <translation>Plus aucun ancien message disponible.</translation>
    </message>
    <message>
        <source>HTTP-error </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No connection possible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotYet</name>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Sorry, this feature isn&apos;t available at the moment.</source>
        <translation>Cette fonctionnalité n'est malheureusement pas encore développée</translation>
    </message>
</context>
<context>
    <name>RedactMessagePage</name>
    <message>
        <source>Redact message</source>
        <translation>Supprimer un message</translation>
    </message>
    <message>
        <source>Reason</source>
        <translation>Raison</translation>
    </message>
</context>
<context>
    <name>RoomMembersPage</name>
    <message>
        <source>Invite new member</source>
        <translation>Inviter un nouveau membre</translation>
    </message>
    <message>
        <source>Members of room</source>
        <translation>Membres du salon</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <source>Enter message or send file</source>
        <translation>Saisir un message ou envoyer un fichier</translation>
    </message>
    <message>
        <source>Load older messages</source>
        <translation>Charger les anciens messages</translation>
    </message>
    <message>
        <source>Invite new member</source>
        <translation type="unfinished">Inviter un nouveau membre</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>Timing options</source>
        <translation>Options temporelle/translation>
    </message>
    <message>
        <source>Timeout Sync</source>
        <translation>Timout Sync</translation>
    </message>
    <message>
        <source>Retry timeout on errors</source>
        <translation>Timeout des nouvelles tentatives après erreurs</translation>
    </message>
    <message>
        <source>Extra timeout between syncs</source>
        <translation>Timeout supplémentaire entre syncronisations</translation>
    </message>
    <message>
        <source>Notification options</source>
        <translation>Options de notification</translation>
    </message>
    <message>
        <source>favourites messages</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>direct chat messages</source>
        <translation>Messages de chat direct</translation>
    </message>
    <message>
        <source>room messages</source>
        <translation>Messages de salon</translation>
    </message>
</context>
<context>
    <name>UploadPage</name>
    <message>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <source>Upload in progress - please wait...</source>
        <translation>Envoi en cours – merci de patienter...</translation>
    </message>
    <message>
        <source>... completed!</source>
        <translation>... terminé!</translation>
    </message>
    <message>
        <source>Upload image</source>
        <translation>Envoi d'image</translation>
    </message>
    <message>
        <source>Upload video</source>
        <translation>Envoi de vidéo</translation>
    </message>
    <message>
        <source>Upload file</source>
        <translation>Envoi de fichier</translation>
    </message>
</context>
</TS>
