<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ChatListItem</name>
    <message>
        <source>Play video</source>
        <translation>Video abspielen</translation>
    </message>
    <message>
        <source>Download file</source>
        <translation>Datei Download</translation>
    </message>
    <message>
        <source>Redact message</source>
        <translation>Nachricht zensieren</translation>
    </message>
    <message>
        <source>Copy message</source>
        <translation>Text kopieren</translation>
    </message>
</context>
<context>
    <name>ConfirmDownloadDialog</name>
    <message>
        <source>Download file</source>
        <translation>Datei Download</translation>
    </message>
    <message>
        <source>File name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <source>If the file already exists, it will be overridden without further confirmation.</source>
        <translation>Falls die Datei bereits existiert, wird sie ohne Rückfrage überschrieben.</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmJoinRoomDialog</name>
    <message>
        <source>Join Room</source>
        <translation>Raum betreten</translation>
    </message>
    <message>
        <source>Please confirm that you want to join the following room:</source>
        <translation>Bitte bestätige, dass du den folgenden Raum betreten möchtest:</translation>
    </message>
</context>
<context>
    <name>ConfirmLeaveRoomDialog</name>
    <message>
        <source>Leaving Room</source>
        <translation>Raum verlassen</translation>
    </message>
    <message>
        <source>Please confirm that you really want to leave the following room:</source>
        <translation>Bitte bestätige, dass du den folgenden Raum verlassen möchtest:</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source> new messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>starting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateDirectRoomDialog</name>
    <message>
        <source>Create direct chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invitee matrix address</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateRoomDialog</name>
    <message>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room topic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Room type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trusted private chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CriticalError</name>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Please close the application.</source>
        <translation>Bitte schließe die Anwendung.</translation>
    </message>
</context>
<context>
    <name>IndexPage</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Überblick</translation>
    </message>
    <message>
        <source>Please wait - loading rooms...</source>
        <translation>Bitte warten - lade Nachrichten...</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favouriten</translation>
    </message>
    <message>
        <source>Direct Chats</source>
        <translation>Gespräche</translation>
    </message>
    <message>
        <source>Rooms</source>
        <translation>Räume</translation>
    </message>
    <message>
        <source>Invitations</source>
        <translation>Einladungen</translation>
    </message>
    <message>
        <source>loading</source>
        <translation>lade</translation>
    </message>
    <message>
        <source>new
messages</source>
        <translation>neue
Nachrichten</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation>Neu Verbinden</translation>
    </message>
    <message>
        <source>Start direct chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>schließen</translation>
    </message>
</context>
<context>
    <name>InviteMemberPage</name>
    <message>
        <source>Invite member</source>
        <translation>Teilnehmer einladen</translation>
    </message>
    <message>
        <source>Full matrix address</source>
        <translation>Vollständige matrix Adresse</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login Account</source>
        <translation>Anmeldung</translation>
    </message>
    <message>
        <source>Homeserver</source>
        <translation>Homeserver</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>anmelden</translation>
    </message>
    <message>
        <source>Login failed with reason:</source>
        <translation>Fehler bei der Anmeldung:</translation>
    </message>
</context>
<context>
    <name>MainController</name>
    <message>
        <source>Cant connect to homeserver. You have to be online on application startup.</source>
        <translation>Keine Verbindung zum Homeserver möglich - du musst beim Start online sein.</translation>
    </message>
    <message>
        <source>Click to open messages</source>
        <translation>Klicke um Nachrichten zu öffnen</translation>
    </message>
    <message>
        <source>online – sleeping </source>
        <translation>online - pausiere </translation>
    </message>
    <message>
        <source>offline - no connection</source>
        <translation>offline - keine Verbindung</translation>
    </message>
    <message>
        <source>offline – code</source>
        <translation>offline - Fehler</translation>
    </message>
    <message>
        <source>You need to be online for this action.</source>
        <translation>Du musst für diese Aktion online sein.</translation>
    </message>
    <message>
        <source>You have to be online for this action.</source>
        <translation>Für dies Aktion musst du online sein.</translation>
    </message>
    <message>
        <source>Download started in background.</source>
        <translation>Download im Hintergrund gestartet.</translation>
    </message>
    <message>
        <source>online – syncing</source>
        <translation>online - synchronisiere</translation>
    </message>
    <message>
        <source>logged out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MatrixClient</name>
    <message>
        <source>Encryption not yet supported ...</source>
        <translation>Verschlüsselung nicht unterstützt...</translation>
    </message>
    <message>
        <source>redacted</source>
        <translation>zensiert</translation>
    </message>
    <message>
        <source>unspecified</source>
        <translation>nicht spezifiziert</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <source>Unknown error during upload, perhaps the filesize is to large.</source>
        <translation>Unbekannter Fehler während dem Upload, eventuell ist die Datei zu groß.</translation>
    </message>
    <message>
        <source>: bad request</source>
        <translation>: ungültige Anfrage</translation>
    </message>
    <message>
        <source>: not authenticated</source>
        <translation>: nicht authentifiziert</translation>
    </message>
    <message>
        <source>: access forbidden</source>
        <translation>: Zugriff verboten</translation>
    </message>
    <message>
        <source>: timeout error</source>
        <translation>: Timeout Fehler</translation>
    </message>
    <message>
        <source>Unknown error while loading old messages.</source>
        <translation>Unbekannter Fehler während des Ladens von alten Nachrichten.</translation>
    </message>
    <message>
        <source>No more older messages available.</source>
        <translation>Keine weiteren alten Nachrichten vorhanden.</translation>
    </message>
    <message>
        <source>HTTP-error </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No connection possible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotYet</name>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Sorry, this feature isn&apos;t available at the moment.</source>
        <translation>Diese Funktion ist leider noch nicht implementiert.</translation>
    </message>
</context>
<context>
    <name>RedactMessagePage</name>
    <message>
        <source>Redact message</source>
        <translation>Nachricht zensieren</translation>
    </message>
    <message>
        <source>Reason</source>
        <translation>Begründung</translation>
    </message>
</context>
<context>
    <name>RoomMembersPage</name>
    <message>
        <source>Invite new member</source>
        <translation>Neuen Teilnehmer einladen</translation>
    </message>
    <message>
        <source>Members of room</source>
        <translation>Mitglieder des Raums</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <source>Enter message or send file</source>
        <translation>Nachricht eingeben oder Datei senden</translation>
    </message>
    <message>
        <source>Load older messages</source>
        <translation>Lade alte Nachrichten</translation>
    </message>
    <message>
        <source>Invite new member</source>
        <translation type="unfinished">Neuen Teilnehmer einladen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Timing options</source>
        <translation>Zeit-Optionen</translation>
    </message>
    <message>
        <source>Timeout Sync</source>
        <translation>Timout Sync</translation>
    </message>
    <message>
        <source>Retry timeout on errors</source>
        <translation>Timeout für Neuverbindung bei Fehlern</translation>
    </message>
    <message>
        <source>Extra timeout between syncs</source>
        <translation>Extra Timeout zwischen zwei Syncs</translation>
    </message>
    <message>
        <source>Notification options</source>
        <translation>Benachrichtigungs-Optionen</translation>
    </message>
    <message>
        <source>favourites messages</source>
        <translation>Favouriten</translation>
    </message>
    <message>
        <source>direct chat messages</source>
        <translation>Gespräche</translation>
    </message>
    <message>
        <source>room messages</source>
        <translation>Räume</translation>
    </message>
</context>
<context>
    <name>UploadPage</name>
    <message>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <source>Upload in progress - please wait...</source>
        <translation>Lade hoch – bitte warten...</translation>
    </message>
    <message>
        <source>... completed!</source>
        <translation>... fertig!</translation>
    </message>
    <message>
        <source>Upload image</source>
        <translation>Bild hochladen</translation>
    </message>
    <message>
        <source>Upload video</source>
        <translation>Video hochladen</translation>
    </message>
    <message>
        <source>Upload file</source>
        <translation>Datei hochladen</translation>
    </message>
</context>
</TS>
